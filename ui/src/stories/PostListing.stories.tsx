import React, { useState } from 'react';
import { MemoryRouter } from 'react-router-dom';
import {
  Box,
  Label,
  Input,
  Select,
  Textarea,
  Radio,
  Flex,
  Checkbox,
  Slider,
} from 'theme-ui';
import Button from '../components/elements/Button';
import { BasePostListing } from '../components/post-listing';
import { ThemeSystemProvider } from '../components/ThemeSystemProvider';
import { themes } from '../theme';

export default { title: 'PostListing' };

const postListingProps = {
  post: {
    id: 2,
    name: 'image post',
    url: 'https://www.chapo.chat/pictrs/image/iBgQD8bSM6.jpg',
    body: 'this is an image post bod',
    creator_id: 2,
    community_id: 2,
    removed: false,
    locked: false,
    published: '2020-08-21T14:30:36.272323',
    updated: null,
    deleted: false,
    nsfw: false,
    stickied: false,
    embed_title: null,
    embed_description: null,
    embed_html: null,
    thumbnail_url: null,
    ap_id: 'https://localhost:8536/post/2',
    local: true,
    creator_actor_id: 'https://localhost:8536/u/greatbearshark',
    creator_local: true,
    creator_name: 'greatbearshark',
    creator_preferred_username: null,
    creator_published: '2020-07-04T21:30:57.470234',
    creator_avatar: null,
    creator_tags: '{pronouns: "he/him,they/them"}',
    creator_community_tags: null,
    banned: false,
    banned_from_community: false,
    community_actor_id: 'https://localhost:8536/c/main',
    community_local: true,
    community_name: 'main',
    community_icon: null,
    community_removed: false,
    community_deleted: false,
    community_nsfw: false,
    number_of_comments: 0,
    score: 1,
    upvotes: 1,
    downvotes: 0,
    hot_rank: 1,
    hot_rank_active: 1,
    newest_activity_time: '2020-08-21T14:30:36.272323',
    user_id: null,
    my_vote: null,
    subscribed: null,
    read: null,
    saved: null,
  },
  showCommunity: true,
  enableDownvotes: true,
  enableNsfw: true,
  history: {
    length: 1,
    action: 'POP',
    location: '{hash: "", pathname: "/", search: "", state: undefi…}',
    createHref: 'ƒ createHref() {}',
    push: 'ƒ push() {}',
    replace: 'ƒ replace() {}',
    go: 'ƒ go() {}',
    goBack: 'ƒ goBack() {}',
    goForward: 'ƒ goForward() {}',
    block: 'ƒ block() {}',
    listen: 'ƒ listen() {}',
  },
  location: {
    pathname: '/',
    search: '',
    hash: '',
  },
  match: {
    path: '/',
    url: '/',
    isExact: true,
    params: '{}',
  },
};

export const Basic = () => {
  return (
    <MemoryRouter>
      Post Listing Stories
      {/* @ts-ignore */}
      <BasePostListing {...postListingProps} />
    </MemoryRouter>
  );
};
