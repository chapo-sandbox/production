import React from 'react';
import Block from './elements/Block';
import { Icon } from './icon';

export default function Spinner() {
  return <Icon name="hexbear" className="main-spinner" size="40px" />;
}

export function SpinnerSection() {
  return (
    <Block
      display="flex"
      justifyContent="center"
      minHeight="100px"
      alignItems="center"
    >
      <Spinner />
    </Block>
  );
}
