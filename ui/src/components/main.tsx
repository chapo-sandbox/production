import React, { Component, useEffect, useState } from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { Subscription } from 'rxjs';
import { retryWhen, delay, take } from 'rxjs/operators';
import useSWR from 'swr';
import {
  UserOperation,
  CommunityUser,
  GetFollowedCommunitiesResponse,
  ListCommunitiesForm,
  ListCommunitiesResponse,
  Community,
  SortType,
  GetSiteResponse,
  ListingType,
  DataType,
  SiteResponse,
  GetPostsResponse,
  PostResponse,
  Post,
  GetPostsForm,
  Comment,
  GetCommentsForm,
  GetCommentsResponse,
  CommentResponse,
  AddAdminResponse,
  AddSitemodResponse,
  BanUserResponse,
  WebSocketJsonResponse,
} from '../interfaces';
import { WebSocketService, UserService } from '../services';
import { PostListings } from './post-listings';
import { CommentNodes } from './comment-nodes';
import { SortSelect } from './sort-select';
import { ListingTypeSelect } from './listing-type-select';
import { DataTypeSelect } from './data-type-select';
import { SiteForm } from './site-form';
import { UserListing } from './user-listing';
import { CommunityLink } from './community-link';
import {
  wsJsonToRes,
  repoUrl,
  mdToHtml,
  fetchLimit,
  toast,
  getListingTypeFromProps,
  getPageFromProps,
  getSortTypeFromProps,
  getDataTypeFromProps,
  editCommentRes,
  saveCommentRes,
  createCommentLikeRes,
  createPostLikeFindRes,
  editPostFindRes,
  commentsToFlatNodes,
  setupTippy,
  isCommentChanged,
  isPostChanged,
  api,
  fetcher,
} from '../utils';
import { BASE_PATH } from '../isProduction';
import { i18n } from '../i18next';
import { Trans, withTranslation } from 'react-i18next';
import { PATREON_URL } from '../constants';
import { Icon } from './icon';
import { linkEvent } from '../linkEvent';
import { Box, Flex } from 'theme-ui';
import Button from './elements/Button';
import Card from './elements/Card';
import Block from './elements/Block';
import Spinner, { SpinnerSection } from './Spinner';
import { useInterval } from '../hooks/useInterval';

interface MainState {
  subscribedCommunities: Array<CommunityUser>;
  trendingCommunities: Array<Community>;
  siteRes: GetSiteResponse;
  showEditSite: boolean;
  loading: false;
  posts: Array<Post>;
  comments: Array<Comment>;
  listingType: ListingType;
  dataType: DataType;
  sort: SortType;
  page: number;
  filtersOpen: boolean;
}

function getMoscowTime(): string {
  const localDate = new Date();

  const utc = localDate.getTime() + localDate.getTimezoneOffset() * 60000;

  // create new Date object for different city
  // using supplied offset
  const moscowTime = new Date(utc + 3600000 * 3);
  return moscowTime.toLocaleString().split(', ')[1];
}

function MoscowTime() {
  const [time, setTime] = useState(null);

  useInterval(() => {
    setTime(getMoscowTime)
  }, 1000, true)

  return (
    <>
      It is currently {time} in Moscow
    </>
  )
}

interface MainProps {
  listingType: ListingType;
  dataType: DataType;
  sort: SortType;
  page: number;
}

interface UrlParams {
  listingType?: string;
  dataType?: string;
  sort?: string;
  page?: number;
}

class Main extends Component<MainProps & RouteComponentProps, MainState> {
  private subscription: Subscription;
  private emptyState: MainState = {
    subscribedCommunities: [],
    trendingCommunities: [],
    siteRes: {
      site: {
        id: null,
        name: null,
        creator_id: null,
        published: null,
        creator_name: null,
        number_of_users: null,
        number_of_posts: null,
        number_of_comments: null,
        number_of_communities: null,
        enable_downvotes: null,
        enable_create_communities: null,
        open_registration: null,
        enable_nsfw: null,
      },
      admins: [],
      sitemods: [],
      banned: [],
      online: null,
    },
    showEditSite: false,
    loading: false,
    posts: [],
    comments: [],
    listingType: getListingTypeFromProps(this.props) || ListingType.Subscribed,
    dataType: getDataTypeFromProps(this.props) || DataType.Post,
    sort: getSortTypeFromProps(this.props) || SortType.Hot,
    page: getPageFromProps(this.props),
    filtersOpen: false,
  };

  constructor(props: any, context: any) {
    super(props, context);

    this.state = this.emptyState;
    this.handleEditCancel = this.handleEditCancel.bind(this);
    this.handleSortChange = this.handleSortChange.bind(this);
    this.handleListingTypeChange = this.handleListingTypeChange.bind(this);
    this.handleDataTypeChange = this.handleDataTypeChange.bind(this);
  }

  componentDidMount() {
    this.subscription = WebSocketService.Instance.subject
      .pipe(retryWhen(errors => errors.pipe(delay(3000), take(10))))
      .subscribe(
        msg => this.parseMessage(msg),
        err => console.error(err),
        () => console.log('complete')
      );
    WebSocketService.Instance.getSite();
    if (UserService.Instance.user) {
      WebSocketService.Instance.getFollowedCommunities();
    }
    let listCommunitiesForm: ListCommunitiesForm = {
      sort: SortType[SortType.Hot],
      limit: 6,
    };
    WebSocketService.Instance.listCommunities(listCommunitiesForm);
    this.fetchData();
  }

  componentWillUnmount() {
    this.subscription.unsubscribe();
  }

  static getDerivedStateFromProps(props: any): MainProps {
    return {
      listingType: getListingTypeFromProps(props),
      dataType: getDataTypeFromProps(props),
      sort: getSortTypeFromProps(props),
      page: getPageFromProps(props),
    };
  }

  componentDidUpdate(_: any, lastState: MainState) {
    if (
      lastState.listingType !== this.state.listingType ||
      lastState.dataType !== this.state.dataType ||
      lastState.sort !== this.state.sort ||
      lastState.page !== this.state.page
    ) {
      // this.setState({ loading: true });
      this.fetchData();
    }
  }

  render() {
    return (
      <div className="container" style={{ maxWidth: '100%' }}>
        <div className="row">
          <main role="main" className="col-12 col-md-8">
            {this.posts()}
          </main>
          <aside className="col-12 col-md-4 sidebar">{this.my_sidebar()}</aside>
        </div>
      </div>
    );
  }

  my_sidebar() {
    return (
      <div>
        {!this.state.loading && (
          <div>
            <Card>
              <div className="card-body">
                {this.trendingCommunities()}
                {UserService.Instance.user &&
                  this.state.subscribedCommunities.length > 0 && (
                    <div>
                      <h5>
                        <Trans i18nKey="subscribed_to_communities">
                          #
                          <Link className="text-body" to="/communities">
                            #
                          </Link>
                        </Trans>
                      </h5>
                      <ul className="list-inline">
                        {this.state.subscribedCommunities.map(community => (
                          <li key={community.id} className="list-inline-item">
                            <CommunityLink
                              community={{
                                name: community.community_name,
                                id: community.community_id,
                                local: community.community_local,
                                actor_id: community.community_actor_id,
                              }}
                            />
                          </li>
                        ))}
                      </ul>
                    </div>
                  )}
                <Flex>
                  <Button
                    css={{ width: '100%', color: '#fff !important' }}
                    as={Link}
                    // @ts-ignore
                    to="/create_post"
                  >
                    {i18n.t('create_post')}
                  </Button>
                </Flex>
                {/* {this.showCreateCommunity() && (
                )} */}
              </div>
            </Card>
            {this.sidebar()}
            {this.donations()}
            {this.landing()}
          </div>
        )}
      </div>
    );
  }

  trendingCommunities() {
    return (
      <div>
        <h5>
          <Trans i18nKey="trending_communities">
            #
            <Link className="text-body" to="/communities">
              #
            </Link>
          </Trans>
        </h5>
        <ul className="list-inline">
          {this.state.trendingCommunities.map(community => (
            <li key={community.id} className="list-inline-item">
              <CommunityLink community={community} />
            </li>
          ))}
        </ul>
      </div>
    );
  }

  sidebar() {
    return (
      <div>
        {!this.state.showEditSite ? (
          this.siteInfo()
        ) : (
          <SiteForm
            site={this.state.siteRes.site}
            onCancel={this.handleEditCancel}
          />
        )}
      </div>
    );
  }

  updateUrl(paramUpdates: UrlParams) {
    const listingTypeStr =
      paramUpdates.listingType ||
      ListingType[this.state.listingType].toLowerCase();
    const dataTypeStr =
      paramUpdates.dataType || DataType[this.state.dataType].toLowerCase();
    const sortStr =
      paramUpdates.sort || SortType[this.state.sort].toLowerCase();
    const page = paramUpdates.page || this.state.page;
    this.props.history.push(
      `/home/data_type/${dataTypeStr}/listing_type/${listingTypeStr}/sort/${sortStr}/page/${page}`
    );
  }

  siteInfo() {
    return (
      <div>
        <div className="card border-secondary mb-3">
          <div className="card-body">
            <h5 className="mb-4 text-center h3">{`${this.state.siteRes.site.name}`}</h5>
            <img
              className="img-fluid mb-2"
              src={`${BASE_PATH}hexbear-logo.png`}
              alt="hexbear logo"
            />
            <img
              src={`${BASE_PATH}welcome.gif`}
              className="img-fluid"
              style={{ width: '100%' }}
              alt="welcome sign"
            />
            {this.canAdmin && (
              <ul className="list-inline mb-1 text-muted font-weight-bold">
                <li className="list-inline-item-action">
                  <span
                    className="pointer"
                    onClick={this.handleEditClick}
                    data-tippy-content={i18n.t('edit')}
                  >
                    <Icon name="edit" />
                  </span>
                </li>
              </ul>
            )}
            <div className="my-2">
              <MoscowTime />
            </div>
            <ul className="my-2 list-inline">
              <li className="list-inline-item user-online-badge mb-1">
                <Icon name="hexbear" className="mr-2" />{' '}
                {i18n.t('number_online', { count: this.state.siteRes.online })}
              </li>
              <li className="list-inline-item badge badge-secondary">
                {i18n.t('number_of_users', {
                  count: this.state.siteRes.site.number_of_users,
                })}
              </li>
              <li className="list-inline-item badge badge-secondary">
                {i18n.t('number_of_communities', {
                  count: this.state.siteRes.site.number_of_communities,
                })}
              </li>
              <li className="list-inline-item badge badge-secondary">
                {i18n.t('number_of_posts', {
                  count: this.state.siteRes.site.number_of_posts,
                })}
              </li>
              <li className="list-inline-item badge badge-secondary">
                {i18n.t('number_of_comments', {
                  count: this.state.siteRes.site.number_of_comments,
                })}
              </li>
              <li className="list-inline-item">
                <Link className="badge badge-secondary" to="/modlog">
                  {i18n.t('modlog')}
                </Link>
              </li>
            </ul>
            <ul className="mt-1 list-inline small mb-0">
              <li className="list-inline-item">{i18n.t('admins')}:</li>
              {this.state.siteRes.admins.map(admin => (
                <li key={admin.id} className="list-inline-item">
                  <UserListing
                    user={{
                      name: admin.name,
                      avatar: admin.avatar,
                      local: admin.local,
                      actor_id: admin.actor_id,
                      id: admin.id,
                    }}
                  />
                </li>
              ))}
            </ul>
            <ul className="mt-1 list-inline small mb-0">
              <li className="list-inline-item">{i18n.t('sitemods')}:</li>
              {this.state.siteRes.sitemods.map(sitemod => (
                <li key={sitemod.id} className="list-inline-item">
                  <UserListing
                    user={{
                      name: sitemod.name,
                      avatar: sitemod.avatar,
                      local: sitemod.local,
                      actor_id: sitemod.actor_id,
                      id: sitemod.id,
                    }}
                  />
                </li>
              ))}
            </ul>
          </div>
        </div>
        {this.state.siteRes.site.description && (
          <div className="card border-secondary mb-3">
            <div className="card-body">
              <div
                className="md-div"
                dangerouslySetInnerHTML={mdToHtml(
                  this.state.siteRes.site.description
                )}
              />
            </div>
          </div>
        )}
      </div>
    );
  }

  donations() {
    return (
      <div className="card border-secondary mb-3">
        <div className="card-body">
          <p>Our Soros stipend only gets us so far.</p>

          <a href={PATREON_URL}>
            <h4>Support Us on Liberapay</h4>
          </a>
        </div>
      </div>
    );
  }

  landing() {
    return (
      <div className="card border-secondary">
        <div className="card-body">
          <h5>
            {i18n.t('powered_by')}
            <svg className="icon mx-2">
              <use xlinkHref="#icon-mouse">#</use>
            </svg>
            <a href={repoUrl}>Lemmy</a>
          </h5>
          <p className="mb-0">
            <Trans i18nKey="landing_0">
              #
              <a href="https://en.wikipedia.org/wiki/Social_network_aggregation">
                #
              </a>
              <a href="https://en.wikipedia.org/wiki/Fediverse">#</a>
              <br className="big" />
              <code>#</code>
              <br />
              <b>#</b>
              <br className="big" />
              <a href={repoUrl}>#</a>
              <br className="big" />
              <a href="https://www.rust-lang.org">#</a>
              <a href="https://actix.rs/">#</a>
              <a href="https://infernojs.org">#</a>
              <a href="https://www.typescriptlang.org/">#</a>
              <br className="big" />
              <a href="https://github.com/LemmyNet/lemmy/graphs/contributors?type=a">
                #
              </a>
            </Trans>
          </p>
        </div>
      </div>
    );
  }

  posts() {
    return (
      <div className="main-content-wrapper">
        {this.selects()}

        {this.state.loading ? (
          <SpinnerSection />
        ) : (
          <div>
            {this.listings()}
            {this.paginator()}
          </div>
        )}
      </div>
    );
  }

  listings() {
    return this.state.dataType == DataType.Post ? (
      <DataWrapper
        page={this.state.page}
        showCommunity
        removeDuplicates
        listingType={ListingType[this.state.listingType]}
        sort={this.state.sort}
        enableDownvotes={this.state.siteRes.site.enable_downvotes}
        enableNsfw={this.state.siteRes.site.enable_nsfw}
        nextPage={this.nextPage}
        prevPage={this.prevPage}
      />
    ) : (
      <CommentNodes
        nodes={commentsToFlatNodes(this.state.comments)}
        noIndent
        showCommunity
        sortType={this.state.sort}
        showContext
        enableDownvotes={this.state.siteRes.site.enable_downvotes}
      />
    );
  }

  selects() {
    const isMobile = window.innerWidth < 768;
    return (
      <div>
        <Block display="flex" alignItems="center" flexWrap="wrap" mb={3}>
          <SortSelect sort={this.state.sort} onChange={this.handleSortChange} />

          {this.state.listingType == ListingType.All && (
            <a
              href={`/feeds/all.xml?sort=${SortType[this.state.sort]}`}
              target="_blank"
              title="RSS"
              rel="noopener"
              className="mr-2"
            >
              <Icon name="rss" />
            </a>
          )}
          {UserService.Instance.user &&
            this.state.listingType == ListingType.Subscribed && (
              <a
                href={`/feeds/front/${UserService.Instance.auth}.xml?sort=${
                  SortType[this.state.sort]
                }`}
                className="mr-2"
                target="_blank"
                title="RSS"
              >
                <Icon name="rss" className="icon text-muted small" />
              </a>
            )}
          {isMobile && (
            <button
              className="btn text-right mr-2"
              onClick={linkEvent(this, this.toggleMobileFilters)}
              style={{ padding: 0 }}
            >
              <svg className="icon text-muted">
                <use xlinkHref="#icon-settings">#</use>
              </svg>
            </button>
          )}
          {isMobile && (
            <Block marginLeft="auto">
              <Link to="/create_post">
                <Button
                  variant="primary"
                  title={i18n.t('create_post')}
                  css={{ paddingBottom: '10px' }}
                >
                  <Icon name="plus" className="icon custom-icon" />
                </Button>
              </Link>
            </Block>
          )}
          {(!isMobile || (isMobile && this.state.filtersOpen)) && (
            <Block display="flex" mt={isMobile ? 3 : 0}>
              <Block mr={2}>
                <ListingTypeSelect
                  type_={this.state.listingType}
                  onChange={this.handleListingTypeChange}
                />
              </Block>
              <Block mr={2}>
                <DataTypeSelect
                  type_={this.state.dataType}
                  onChange={this.handleDataTypeChange}
                />
              </Block>
            </Block>
          )}
        </Block>
      </div>
    );
  }

  paginator() {
    // Post listings use their own paginator
    if (this.state.dataType === DataType.Post) {
      return null;
    }

    return (
      <div className="my-2">
        {this.state.page > 1 && (
          <Button mr={1} variant="muted" onClick={this.prevPage}>
            {i18n.t('prev')}
          </Button>
        )}
        {this.state.posts.length > 0 && (
          <Button variant="muted" onClick={this.nextPage}>
            {i18n.t('next')}
          </Button>
        )}
      </div>
    );
  }

  get canAdmin(): boolean {
    return (
      UserService.Instance.user &&
      this.state.siteRes.admins
        .map(a => a.id)
        .includes(UserService.Instance.user.id)
    );
  }

  toggleMobileFilters(i: Main) {
    i.setState(prevState => ({
      filtersOpen: !prevState.filtersOpen,
    }));
  }

  handleEditClick = () => {
    this.setState({
      showEditSite: true,
    });
  };

  handleEditCancel() {
    this.setState({
      showEditSite: false,
    });
  }

  nextPage = () => {
    this.updateUrl({ page: this.state.page + 1 });
    window.scrollTo(0, 0);
  };

  prevPage = () => {
    this.updateUrl({ page: this.state.page - 1 });
    window.scrollTo(0, 0);
  };

  handleSortChange(val: SortType) {
    this.updateUrl({ sort: SortType[val].toLowerCase(), page: 1 });
    window.scrollTo(0, 0);
  }

  handleListingTypeChange(val: ListingType) {
    this.updateUrl({ listingType: ListingType[val].toLowerCase(), page: 1 });
    window.scrollTo(0, 0);
  }

  handleDataTypeChange(val: DataType) {
    this.updateUrl({ dataType: DataType[val].toLowerCase(), page: 1 });
    window.scrollTo(0, 0);
  }

  async fetchData() {
    if (this.state.dataType == DataType.Post) {
      let getPostsForm: GetPostsForm = {
        page: this.state.page,
        limit: fetchLimit,
        sort: SortType[this.state.sort],
        type_: ListingType[this.state.listingType],
      };
      // WebSocketService.Instance.getPosts(getPostsForm);
      // const params = new URLSearchParams({
      //   page: this.state.page,
      //   limit: fetchLimit,
      //   sort: SortType[this.state.sort],
      //   type_: ListingType[this.state.listingType],
      // })
      // console.log(params.toString())
      // const res = await api.get(`post/list?${params.toString()}`);
      // console.log({ res })
    } else {
      let getCommentsForm: GetCommentsForm = {
        page: this.state.page,
        limit: fetchLimit,
        sort: SortType[this.state.sort],
        type_: ListingType[this.state.listingType],
      };
      WebSocketService.Instance.getComments(getCommentsForm);
    }
  }

  showCreateCommunity() {
    if (this.canAdmin) return true;

    return this.state.siteRes.site.enable_create_communities;
  }

  parseMessage(msg: WebSocketJsonResponse) {
    console.log(msg);
    let res = wsJsonToRes(msg);
    if (msg.error) {
      toast(i18n.t(msg.error), 'danger');
      return;
    } else if (msg.reconnect) {
      this.fetchData();
    } else if (res.op === UserOperation.GetFollowedCommunities) {
      let data = res.data as GetFollowedCommunitiesResponse;
      this.setState({
        subscribedCommunities: data.communities,
      });
    } else if (res.op === UserOperation.ListCommunities) {
      let data = res.data as ListCommunitiesResponse;
      this.setState({
        trendingCommunities: data.communities,
      });
    } else if (res.op === UserOperation.GetSite) {
      let data = res.data as GetSiteResponse;
      // This means it hasn't been set up yet
      if (!data.site) {
        this.props.history.push('/setup');
      } else {
        console.log('data.site is missing', data.site);
        this.setState(
          {
            siteRes: {
              admins: data.admins,
              sitemods: data.sitemods,
              site: data.site,
              banned: data.banned,
              online: data.online,
            },
          },
          () => {
            document.title = `${this.state.siteRes.site.name}`;
          }
        );
      }
    } else if (res.op === UserOperation.EditSite) {
      let data = res.data as SiteResponse;
      this.setState(
        {
          siteRes: {
            ...this.state.siteRes,
            site: data.site,
          },
          showEditSite: false,
        },
        () => {
          toast(i18n.t('site_saved'));
        }
      );
    } else if (res.op === UserOperation.GetPosts) {
      let data = res.data as GetPostsResponse;
      this.setState(
        {
          posts: data.posts,
          loading: false,
        },
        () => {
          setupTippy();
        }
      );
    } else if (res.op === UserOperation.CreatePost) {
      let data = res.data as PostResponse;

      // If you're on subscribed, only push it if you're subscribed.
      if (this.state.listingType === ListingType.Subscribed) {
        if (
          this.state.subscribedCommunities
            .map(c => c.community_id)
            .includes(data.post.community_id)
        ) {
          this.state.posts.unshift(data.post);
        }
      } else {
        // NSFW posts
        let nsfw = data.post.nsfw || data.post.community_nsfw;

        // Don't push the post if its nsfw, and don't have that setting on
        if (
          !nsfw ||
          (nsfw &&
            UserService.Instance.user &&
            UserService.Instance.user.show_nsfw)
        ) {
          this.state.posts.unshift(data.post);
        }
      }
      this.setState(this.state);
    } else if (isPostChanged(res.op)) {
      let data = res.data as PostResponse;
      editPostFindRes(data, this.state.posts);
      this.setState(this.state);
    } else if (res.op === UserOperation.CreatePostLike) {
      let data = res.data as PostResponse;
      createPostLikeFindRes(data, this.state.posts);
      this.setState(this.state);
    } else if (res.op === UserOperation.AddAdmin) {
      let data = res.data as AddAdminResponse;
      this.setState({
        siteRes: {
          ...this.state.siteRes,
          admins: data.admins,
        },
      });
    } else if (res.op === UserOperation.AddSitemod) {
      let data = res.data as AddSitemodResponse;
      this.setState({
        siteRes: {
          ...this.state.siteRes,
          sitemods: data.sitemods,
        },
      });
    } else if (res.op === UserOperation.BanUser) {
      let data = res.data as BanUserResponse;
      let found = this.state.siteRes.banned.find(u => (u.id = data.user.id));

      // Remove the banned if its found in the list, and the action is an unban
      if (found && !data.banned) {
        this.state.siteRes.banned = this.state.siteRes.banned.filter(
          i => i.id !== data.user.id
        );
      } else {
        this.state.siteRes.banned.push(data.user);
      }

      this.state.posts
        .filter(p => p.creator_id == data.user.id)
        .forEach(p => (p.banned = data.banned));

      this.setState(this.state);
    } else if (res.op === UserOperation.GetComments) {
      let data = res.data as GetCommentsResponse;
      this.setState({
        comments: data.comments,
        loading: false,
      });
    } else if (isCommentChanged(res.op)) {
      let data = res.data as CommentResponse;
      editCommentRes(data, this.state.comments);
      this.setState(this.state);
    } else if (res.op == UserOperation.CreateComment) {
      let data = res.data as CommentResponse;

      // Necessary since it might be a user reply
      if (data.recipient_ids.length == 0) {
        // If you're on subscribed, only push it if you're subscribed.
        if (this.state.listingType == ListingType.Subscribed) {
          if (
            this.state.subscribedCommunities
              .map(c => c.community_id)
              .includes(data.comment.community_id)
          ) {
            this.state.comments.unshift(data.comment);
          }
        } else {
          this.state.comments.unshift(data.comment);
        }
        this.setState(this.state);
      }
    } else if (res.op == UserOperation.SaveComment) {
      let data = res.data as CommentResponse;
      saveCommentRes(data, this.state.comments);
      this.setState(this.state);
    } else if (res.op == UserOperation.CreateCommentLike) {
      let data = res.data as CommentResponse;
      createCommentLikeRes(data, this.state.comments);
      this.setState(this.state);
    }
  }
}

function DataWrapper({
  page,
  sort,
  listingType,
  nextPage,
  prevPage,
  ...props
}) {
  const params = new URLSearchParams({
    page: page,
    limit: fetchLimit,
    sort: SortType[sort],
    type_: listingType,
  } as any);

  if (UserService.Instance.auth) {
    params.append('auth', UserService.Instance.auth);
  }

  const { data, error } = useSWR(`post/list?${params.toString()}`, fetcher);

  if (!data) {
    return <SpinnerSection />;
  }

  return (
    <>
      {/* @ts-ignore */}
      <PostListings posts={data.posts} sort={sort} {...props} />
      <Box my={4}>
        {page > 1 && (
          <Button mr={1} variant="muted" onClick={prevPage}>
            {i18n.t('prev')}
          </Button>
        )}
        {data?.posts.length > 0 && (
          <Button variant="muted" onClick={nextPage}>
            {i18n.t('next')}
          </Button>
        )}
      </Box>
    </>
  );
}

// @ts-ignore
export default withTranslation()(withRouter(Main));
