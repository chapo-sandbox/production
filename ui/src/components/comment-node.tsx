import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  CommentNode as CommentNodeI,
  CommentLikeForm,
  CommentForm as CommentFormI,
  EditUserMentionForm,
  SaveCommentForm,
  BanFromCommunityForm,
  BanUserForm,
  CommunityUser,
  UserView,
  AddModToCommunityForm,
  AddAdminForm,
  AddSitemodForm,
  TransferCommunityForm,
  TransferSiteForm,
  BanType,
  CommentSortType,
  SortType,
  DeleteCommentForm,
  RemoveCommentForm,
  MarkCommentReadForm,
} from '../interfaces';
import { WebSocketService, UserService } from '../services';
import {
  mdToHtml,
  getUnixTime,
  canMod,
  isMod,
  setupTippy,
  colorList,
  imagesDownsize,
  replaceImageEmbeds,
} from '../utils';
import moment from 'moment';
import { MomentTime } from './moment-time';
import { CommentForm } from './comment-form';
import { CommentNodes } from './comment-nodes';
import { UserListing } from './user-listing';
import { CommunityLink } from './community-link';
import { i18n } from '../i18next';
import { Icon } from './icon';
import { linkEvent } from '../linkEvent';
import { RoleBadge } from './RoleBadge';

interface CommentNodeState {
  showReply: boolean;
  showEdit: boolean;
  showRemoveDialog: boolean;
  removeReason: string;
  showBanDialog: boolean;
  banReason: string;
  reportReason: string;
  banExpires: string;
  banType: BanType;
  showConfirmTransferSite: boolean;
  showConfirmTransferCommunity: boolean;
  showConfirmAppointAsMod: boolean;
  showConfirmAppointAsAdmin: boolean;
  showConfirmAppointAsSitemod: boolean;
  showReportDialog: boolean;
  collapsed: boolean;
  viewSource: boolean;
  showAdvanced: boolean;
  my_vote: number;
  score: number;
  upvotes: number;
  downvotes: number;
  borderColor: string;
  readLoading: boolean;
  saveLoading: boolean;
}

interface CommentNodeProps {
  node: CommentNodeI;
  noIndent?: boolean;
  viewOnly?: boolean;
  locked?: boolean;
  markable?: boolean;
  showContext?: boolean;
  moderators: Array<CommunityUser>;
  admins: Array<UserView>;
  sitemods: Array<UserView>;
  // TODO is this necessary, can't I get it from the node itself?
  postCreatorId?: number;
  showCommunity?: boolean;
  sort?: CommentSortType;
  sortType?: SortType;
  enableDownvotes: boolean;
}

export class CommentNode extends Component<CommentNodeProps, CommentNodeState> {
  private emptyState: CommentNodeState = {
    showReply: false,
    showEdit: false,
    showRemoveDialog: false,
    removeReason: null,
    showBanDialog: false,
    banReason: null,
    reportReason: null,
    banExpires: null,
    banType: BanType.Community,
    collapsed: false,
    viewSource: false,
    showAdvanced: false,
    showConfirmTransferSite: false,
    showConfirmTransferCommunity: false,
    showConfirmAppointAsMod: false,
    showConfirmAppointAsAdmin: false,
    showConfirmAppointAsSitemod: false,
    showReportDialog: false,
    my_vote: this.props.node.comment.my_vote,
    score: this.props.node.comment.score,
    upvotes: this.props.node.comment.upvotes,
    downvotes: this.props.node.comment.downvotes,
    borderColor: this.props.node.comment.depth
      ? colorList[this.props.node.comment.depth % colorList.length]
      : colorList[0],
    readLoading: false,
    saveLoading: false,
  };

  state = this.emptyState;

  constructor(props: any, context: any) {
    super(props, context);

    if (
      this.props.node.comment.removed ||
      this.props.node.comment.banned ||
      this.props.node.comment.banned_from_community
    ) {
      this.state.collapsed = true;
    }

    this.handleReplyCancel = this.handleReplyCancel.bind(this);
    this.handleCommentUpvote = this.handleCommentUpvote.bind(this);
    this.handleCommentDownvote = this.handleCommentDownvote.bind(this);
  }

  UNSAFE_componentWillReceiveProps(nextProps: CommentNodeProps) {
    this.setState({
      my_vote: nextProps.node.comment.my_vote,
      upvotes: nextProps.node.comment.upvotes,
      downvotes: nextProps.node.comment.downvotes,
      score: nextProps.node.comment.score,
      readLoading: false,
      saveLoading: false,
    });
  }

  render() {
    let node = this.props.node;

    const borderStyle =
      !this.props.noIndent && this.props.node.comment.parent_id
        ? {
            borderLeftColor: this.state.borderColor,
            borderLeftWidth: `2px`,
            borderLeftStyle: `solid`,
          }
        : {};

    return (
      <div
        className={`comment ${
          node.comment.parent_id && !this.props.noIndent ? 'ml-2' : ''
        }`}
      >
        <div
          id={`comment-${node.comment.id}`}
          className={`details comment-node border-top py-2 ${
            this.isCommentNew ? 'mark' : ''
          }`}
          // @ts-ignore
          style={borderStyle}
        >
          <div
            className={`${
              !this.props.noIndent &&
              this.props.node.comment.parent_id &&
              'ml-2'
            }`}
          >
            <div className="d-flex flex-wrap align-items-center text-muted small">
              <div
                className="unselectable pointer mr-2 comment-expand-button"
                onClick={linkEvent(this, this.handleCommentCollapse)}
              >
                {this.state.collapsed ? (
                  <Icon name="plus" size="16px" />
                ) : (
                  <Icon name="minus" size="16px" />
                )}
              </div>
              <span className="mr-2">
                <UserListing
                  user={{
                    name: node.comment.creator_name,
                    avatar: node.comment.creator_avatar,
                    id: node.comment.creator_id,
                    local: node.comment.creator_local,
                    actor_id: node.comment.creator_actor_id,
                    published: node.comment.creator_published,
                  }}
                  isMod={this.isMod}
                  isAdmin={this.isAdmin}
                  isSitemod={this.isSitemod}
                />
              </span>
              {this.isAdmin && (
                <RoleBadge role="admin" tooltipText={i18n.t('admin')}>
                  {i18n.t('admin')[0]}
                </RoleBadge>
              )}
              {this.isSitemod && (
                <RoleBadge role="sitemod" tooltipText={i18n.t('sitemod')}>
                  {i18n.t('sitemod')[0]}
                </RoleBadge>
              )}
              {this.isMod && !this.isAdmin && !this.isSitemod && (
                <RoleBadge role="mod" tooltipText={i18n.t('mod')}>
                  {i18n.t('mod')[0]}
                </RoleBadge>
              )}
              {node.comment.creator_tags?.pronouns ? (
                <span className="badge mr-1 comment-badge pronouns-badge">
                  {node.comment.creator_tags.pronouns.split(',').join('/')}
                </span>
              ) : null}
              {this.isPostCreator && (
                <RoleBadge role="creator" tooltipText={i18n.t('creator')}>
                  <Icon name="hexagon" />
                </RoleBadge>
              )}
              {(node.comment.banned_from_community || node.comment.banned) && (
                <div className="badge badge-danger mr-2">
                  {i18n.t('banned')}
                </div>
              )}
              {this.props.showCommunity && (
                <>
                  <span className="mx-1">{i18n.t('to')}</span>
                  <CommunityLink
                    community={{
                      name: node.comment.community_name,
                      id: node.comment.community_id,
                      local: node.comment.community_local,
                      actor_id: node.comment.community_actor_id,
                    }}
                  />
                  <span className="mx-2">•</span>
                  <Link className="mr-2" to={`/post/${node.comment.post_id}`}>
                    {node.comment.post_name}
                  </Link>
                </>
              )}
              {/* This is an expanding spacer for mobile */}
              {/* <div className="mr-lg-4 flex-grow-1 flex-lg-grow-0 unselectable pointer mx-2"></div> */}
              <button
                className={`btn unselectable pointer px-2 ${this.scoreColor}`}
                onClick={linkEvent(node, this.handleCommentUpvote)}
                data-tippy-content={this.pointsTippy}
              >
                <Icon name="hexbear" className="icon mr-1 mb-1" />
                <span className="mr-1">{this.state.score}</span>
              </button>
              <span className="mr-1">•</span>
              <span>
                <MomentTime data={node.comment} />
              </span>
            </div>
            {/* end of user row */}
            {this.state.showEdit && (
              <CommentForm
                node={node}
                edit
                onReplyCancel={this.handleReplyCancel}
                disabled={this.props.locked}
                focus
              />
            )}
            {!this.state.showEdit && !this.state.collapsed && (
              <div>
                {this.state.viewSource ? (
                  <pre>{this.commentUnlessRemoved}</pre>
                ) : (
                  <div
                    className="md-div comment-text-container"
                    dangerouslySetInnerHTML={this.formatInnerHTML(
                      this.commentUnlessRemoved
                    )}
                  />
                )}
                <div className="d-flex justify-content-between justify-content-lg-start flex-wrap text-muted font-weight-bold">
                  {this.props.showContext && this.linkBtn}
                  {this.props.markable && (
                    <button
                      className="btn btn-link btn-animate text-muted"
                      onClick={this.handleMarkRead}
                      data-tippy-content={
                        node.comment.read
                          ? i18n.t('mark_as_unread')
                          : i18n.t('mark_as_read')
                      }
                    >
                      {this.state.readLoading ? (
                        this.loadingIcon
                      ) : (
                        <svg
                          className={`icon icon-inline ${
                            node.comment.read && 'text-success'
                          }`}
                        >
                          <use xlinkHref="#icon-check" />
                        </svg>
                      )}
                    </button>
                  )}
                  {UserService.Instance.user && !this.props.viewOnly && (
                    <>
                      <button
                        className={`btn btn-link btn-animate ${
                          this.state.my_vote == 1 ? 'text-info' : 'text-muted'
                        }`}
                        onClick={linkEvent(node, this.handleCommentUpvote)}
                        data-tippy-content={i18n.t('upvote')}
                      >
                        <Icon name="upvote" />
                        {this.state.upvotes !== this.state.score && (
                          <span className="ml-1">{this.state.upvotes}</span>
                        )}
                      </button>
                      {this.props.enableDownvotes && (
                        <button
                          className={`btn btn-link btn-animate ${
                            this.state.my_vote == -1
                              ? 'text-danger'
                              : 'text-muted'
                          }`}
                          onClick={linkEvent(node, this.handleCommentDownvote)}
                          data-tippy-content={i18n.t('downvote')}
                        >
                          <Icon name="downvote" />
                          {this.state.upvotes !== this.state.score && (
                            <span className="ml-1">{this.state.downvotes}</span>
                          )}
                        </button>
                      )}
                      <button
                        className="btn btn-link btn-animate text-muted"
                        onClick={linkEvent(this, this.handleReplyClick)}
                        data-tippy-content={i18n.t('reply')}
                      >
                        <Icon name="reply" />
                      </button>
                      {!this.state.showAdvanced ? (
                        <button
                          className="btn btn-link btn-animate text-muted"
                          onClick={linkEvent(this, this.handleShowAdvanced)}
                          data-tippy-content={i18n.t('more')}
                        >
                          <Icon name="more" />
                        </button>
                      ) : (
                        <>
                          {!this.myComment && (
                            <button className="btn btn-link btn-animate">
                              <Link
                                className="text-muted"
                                to={`/create_private_message?recipient_id=${node.comment.creator_id}`}
                                title={i18n.t('message').toLowerCase()}
                              >
                                <svg className="icon">
                                  <use xlinkHref="#icon-mail" />
                                </svg>
                              </Link>
                            </button>
                          )}
                          {!this.props.showContext && this.linkBtn}
                          <button
                            className="btn btn-link btn-animate text-muted"
                            onClick={linkEvent(
                              this,
                              this.handleSaveCommentClick
                            )}
                            data-tippy-content={
                              node.comment.saved
                                ? i18n.t('unsave')
                                : i18n.t('save')
                            }
                          >
                            {this.state.saveLoading ? (
                              this.loadingIcon
                            ) : (
                              <Icon
                                className={`icon icon-inline ${
                                  node.comment.saved && 'text-warning'
                                }`}
                                name="star"
                              />
                            )}
                          </button>
                          <button
                            className="btn btn-link btn-animate text-muted"
                            onClick={linkEvent(this, this.handleViewSource)}
                            data-tippy-content={i18n.t('view_source')}
                          >
                            <svg
                              className={`icon icon-inline ${
                                this.state.viewSource && 'text-success'
                              }`}
                            >
                              <use xlinkHref="#icon-file-text" />
                            </svg>
                          </button>
                          {this.myComment ? (
                            <>
                              <button
                                className="btn btn-link btn-animate text-muted"
                                onClick={linkEvent(this, this.handleEditClick)}
                                data-tippy-content={i18n.t('edit')}
                              >
                                <Icon name="edit" />
                              </button>
                              <button
                                className="btn btn-link btn-animate text-muted"
                                onClick={linkEvent(
                                  this,
                                  this.handleDeleteClick
                                )}
                                data-tippy-content={
                                  !node.comment.deleted
                                    ? i18n.t('delete')
                                    : i18n.t('restore')
                                }
                              >
                                <svg
                                  className={`icon icon-inline ${
                                    node.comment.deleted && 'text-danger'
                                  }`}
                                >
                                  <use xlinkHref="#icon-trash" />
                                </svg>
                              </button>
                            </>
                          ) : (
                            <button
                              className="btn btn-sm btn-link btn-animate text-muted small"
                              onClick={linkEvent(
                                this,
                                this.handleReportComment
                              )}
                              data-tippy-content={i18n.t('snitch')}
                            >
                              <Icon name="report" />
                            </button>
                          )}
                          {/* Admins, sitemods, and mods can remove comments */}
                          {(this.canMod || this.canAdmin) && (
                            <>
                              {!node.comment.removed ? (
                                <button
                                  className="btn btn-link btn-animate text-muted"
                                  onClick={linkEvent(
                                    this,
                                    this.handleModRemoveShow
                                  )}
                                >
                                  {i18n.t('remove')}
                                </button>
                              ) : (
                                <button
                                  className="btn btn-link btn-animate text-muted"
                                  onClick={this.handleModRemoveSubmit}
                                >
                                  {i18n.t('restore')}
                                </button>
                              )}
                            </>
                          )}
                          {/* Mods can ban from community, and appoint as mods to community */}
                          {this.canMod && (
                            <>
                              {!this.isMod &&
                                (!node.comment.banned_from_community ? (
                                  <button
                                    className="btn btn-link btn-animate text-muted"
                                    onClick={linkEvent(
                                      this,
                                      this.handleModBanFromCommunityShow
                                    )}
                                  >
                                    {i18n.t('ban')}
                                  </button>
                                ) : (
                                  <button
                                    className="btn btn-link btn-animate text-muted"
                                    onClick={linkEvent(
                                      this,
                                      this.handleModBanFromCommunitySubmit
                                    )}
                                  >
                                    {i18n.t('unban')}
                                  </button>
                                ))}
                              {!node.comment.banned_from_community &&
                                (!this.state.showConfirmAppointAsMod ? (
                                  <button
                                    className="btn btn-link btn-animate text-muted"
                                    onClick={linkEvent(
                                      this,
                                      this.handleShowConfirmAppointAsMod
                                    )}
                                  >
                                    {this.isMod
                                      ? i18n.t('remove_as_mod')
                                      : i18n.t('appoint_as_mod')}
                                  </button>
                                ) : (
                                  <>
                                    <button className="btn btn-link btn-animate text-muted">
                                      {i18n.t('are_you_sure')}
                                    </button>
                                    <button
                                      className="btn btn-link btn-animate text-muted"
                                      onClick={linkEvent(
                                        this,
                                        this.handleAddModToCommunity
                                      )}
                                    >
                                      {i18n.t('yes')}
                                    </button>
                                    <button
                                      className="btn btn-link btn-animate text-muted"
                                      onClick={linkEvent(
                                        this,
                                        this.handleCancelConfirmAppointAsMod
                                      )}
                                    >
                                      {i18n.t('no')}
                                    </button>
                                  </>
                                ))}
                            </>
                          )}
                          {/* Community creators and admins can transfer community to another mod */}
                          {(this.amCommunityCreator || this.canAdmin) &&
                            this.isMod &&
                            (!this.state.showConfirmTransferCommunity ? (
                              <button
                                className="btn btn-link btn-animate text-muted"
                                onClick={linkEvent(
                                  this,
                                  this.handleShowConfirmTransferCommunity
                                )}
                              >
                                {i18n.t('transfer_community')}
                              </button>
                            ) : (
                              <>
                                <button className="btn btn-link btn-animate text-muted">
                                  {i18n.t('are_you_sure')}
                                </button>
                                <button
                                  className="btn btn-link btn-animate text-muted"
                                  onClick={linkEvent(
                                    this,
                                    this.handleTransferCommunity
                                  )}
                                >
                                  {i18n.t('yes')}
                                </button>
                                <button
                                  className="btn btn-link btn-animate text-muted"
                                  onClick={linkEvent(
                                    this,
                                    this
                                      .handleCancelShowConfirmTransferCommunity
                                  )}
                                >
                                  {i18n.t('no')}
                                </button>
                              </>
                            ))}
                          {/* Admins and sitmods can ban from all */}
                          {(this.canAdmin || this.canSitemod) && (
                            <>
                              {!this.isAdmin &&
                                (!node.comment.banned ? (
                                  <button
                                    className="btn btn-link btn-animate text-muted"
                                    onClick={linkEvent(
                                      this,
                                      this.handleModBanShow
                                    )}
                                  >
                                    {i18n.t('ban_from_site')}
                                  </button>
                                ) : (
                                  <button
                                    className="btn btn-link btn-animate text-muted"
                                    onClick={linkEvent(
                                      this,
                                      this.handleModBanSubmit
                                    )}
                                  >
                                    {i18n.t('unban_from_site')}
                                  </button>
                                ))}
                            </>
                          )}
                          {/* Admins can appoint admins and sitemods */}
                          {this.canAdmin && (
                            <>
                              {!node.comment.banned &&
                                (!this.state.showConfirmAppointAsAdmin ? (
                                  <button
                                    className="btn btn-link btn-animate text-muted"
                                    onClick={linkEvent(
                                      this,
                                      this.handleShowConfirmAppointAsAdmin
                                    )}
                                  >
                                    {this.isAdmin
                                      ? i18n.t('remove_as_admin')
                                      : i18n.t('appoint_as_admin')}
                                  </button>
                                ) : (
                                  <>
                                    <button className="btn btn-link btn-animate text-muted">
                                      {i18n.t('are_you_sure')}
                                    </button>
                                    <button
                                      className="btn btn-link btn-animate text-muted"
                                      onClick={linkEvent(
                                        this,
                                        this.handleAddAdmin
                                      )}
                                    >
                                      {i18n.t('yes')}
                                    </button>
                                    <button
                                      className="btn btn-link btn-animate text-muted"
                                      onClick={linkEvent(
                                        this,
                                        this.handleCancelConfirmAppointAsAdmin
                                      )}
                                    >
                                      {i18n.t('no')}
                                    </button>
                                  </>
                                ))}
                              {!node.comment.banned &&
                                (!this.state.showConfirmAppointAsSitemod ? (
                                  <button
                                    className="btn btn-link btn-animate text-muted"
                                    onClick={linkEvent(
                                      this,
                                      this.handleShowConfirmAppointAsSitemod
                                    )}
                                  >
                                    {this.isSitemod
                                      ? i18n.t('remove_as_sitemod')
                                      : i18n.t('appoint_as_sitemod')}
                                  </button>
                                ) : (
                                  <>
                                    <button className="btn btn-link btn-animate text-muted">
                                      {i18n.t('are_you_sure')}
                                    </button>
                                    <button
                                      className="btn btn-link btn-animate text-muted"
                                      onClick={linkEvent(
                                        this,
                                        this.handleAddSitemod
                                      )}
                                    >
                                      {i18n.t('yes')}
                                    </button>
                                    <button
                                      className="btn btn-link btn-animate text-muted"
                                      onClick={linkEvent(
                                        this,
                                        this.handleCancelConfirmAppointAsSitemod
                                      )}
                                    >
                                      {i18n.t('no')}
                                    </button>
                                  </>
                                ))}
                            </>
                          )}
                          {/* Site Creator can transfer to another admin */}
                          {this.amSiteCreator &&
                            this.isAdmin &&
                            (!this.state.showConfirmTransferSite ? (
                              <button
                                className="btn btn-link btn-animate text-muted"
                                onClick={linkEvent(
                                  this,
                                  this.handleShowConfirmTransferSite
                                )}
                              >
                                {i18n.t('transfer_site')}
                              </button>
                            ) : (
                              <>
                                <button className="btn btn-link btn-animate text-muted">
                                  {i18n.t('are_you_sure')}
                                </button>
                                <button
                                  className="btn btn-link btn-animate text-muted"
                                  onClick={linkEvent(
                                    this,
                                    this.handleTransferSite
                                  )}
                                >
                                  {i18n.t('yes')}
                                </button>
                                <button
                                  className="btn btn-link btn-animate text-muted"
                                  onClick={linkEvent(
                                    this,
                                    this.handleCancelShowConfirmTransferSite
                                  )}
                                >
                                  {i18n.t('no')}
                                </button>
                              </>
                            ))}
                        </>
                      )}
                    </>
                  )}
                </div>
                {/* end of button group */}
              </div>
            )}
          </div>
        </div>
        {/* end of details */}
        {this.state.showRemoveDialog && (
          <form className="form-inline" onSubmit={this.handleModRemoveSubmit}>
            <input
              type="text"
              className="form-control mr-2"
              placeholder={i18n.t('reason')}
              value={this.state.removeReason}
              onInput={linkEvent(this, this.handleModRemoveReasonChange)}
            />
            <button type="submit" className="btn btn-secondary">
              {i18n.t('remove_comment')}
            </button>
          </form>
        )}
        {this.state.showBanDialog && (
          <form onSubmit={linkEvent(this, this.handleModBanBothSubmit)}>
            <div className="form-group row">
              <label className="col-form-label">{i18n.t('reason')}</label>
              <input
                type="text"
                className="form-control mr-2"
                placeholder={i18n.t('reason')}
                value={this.state.banReason}
                onInput={linkEvent(this, this.handleModBanReasonChange)}
              />
            </div>
            {/* TODO hold off on expires until later */}
            {/* <div className="form-group row"> */}
            {/*   <label className="col-form-label">Expires</label> */}
            {/*   <input type="date" className="form-control mr-2" placeholder={i18n.t('expires')} value={this.state.banExpires} onInput={linkEvent(this, this.handleModBanExpiresChange)} /> */}
            {/* </div> */}
            <div className="form-group row">
              <button type="submit" className="btn btn-secondary">
                {i18n.t('ban')} {node.comment.creator_name}
              </button>
            </div>
          </form>
        )}
        {this.state.showReportDialog && (
          <form
            onSubmit={linkEvent(this, this.handleReportSubmit)}
            className="px-2"
          >
            <div className="form-group row">
              <label className="col-form-label" htmlFor="comment-report-dialog">
                {i18n.t('reason')}
              </label>
              <input
                id="comment-report-dialog"
                type="text"
                className="form-control mr-2"
                placeholder={i18n.t('reason')}
                value={this.state.reportReason}
                onInput={linkEvent(this, this.handleReportReasonChange)}
                maxLength={600}
              />
            </div>
            <div className="form-group row">
              <button type="submit" className="btn btn-secondary">
                {i18n.t('submit_report')}
              </button>
            </div>
          </form>
        )}
        {this.state.showReply && (
          <CommentForm
            node={node}
            onReplyCancel={this.handleReplyCancel}
            disabled={this.props.locked}
            focus
          />
        )}
        {node.children && !this.state.collapsed && (
          <CommentNodes
            nodes={node.children}
            locked={this.props.locked}
            moderators={this.props.moderators}
            admins={this.props.admins}
            sitemods={this.props.sitemods}
            postCreatorId={this.props.postCreatorId}
            sort={this.props.sort}
            sortType={this.props.sortType}
            enableDownvotes={this.props.enableDownvotes}
          />
        )}
        {/* A collapsed clearfix */}
        {this.state.collapsed && <div className="row col-12" />}
      </div>
    );
  }

  get linkBtn() {
    let node = this.props.node;
    return (
      <Link
        className="btn btn-link btn-animate text-muted"
        to={`/post/${node.comment.post_id}/comment/${node.comment.id}`}
        title={this.props.showContext ? i18n.t('show_context') : i18n.t('link')}
      >
        <svg className="icon icon-inline">
          <use xlinkHref="#icon-link" />
        </svg>
      </Link>
    );
  }

  get loadingIcon() {
    return (
      <svg className="icon icon-spinner spin">
        <use xlinkHref="#icon-spinner" />
      </svg>
    );
  }

  get myComment(): boolean {
    return (
      UserService.Instance.user &&
      this.props.node.comment.creator_id == UserService.Instance.user.id
    );
  }

  get isMod(): boolean {
    return (
      this.props.moderators &&
      isMod(
        this.props.moderators.map(m => m.user_id),
        this.props.node.comment.creator_id
      )
    );
  }

  get isAdmin(): boolean {
    return (
      this.props.admins &&
      isMod(
        this.props.admins.map(a => a.id),
        this.props.node.comment.creator_id
      )
    );
  }

  get isSitemod(): boolean {
    return (
      this.props.sitemods &&
      isMod(
        this.props.sitemods.map(s => s.id),
        this.props.node.comment.creator_id
      )
    );
  }

  get isPostCreator(): boolean {
    return this.props.node.comment.creator_id == this.props.postCreatorId;
  }

  get canMod(): boolean {
    if (this.props.admins && this.props.sitemods && this.props.moderators) {
      let adminsThenSitemodsThenMods = this.props.admins
        .map(a => a.id)
        .concat(this.props.sitemods.map(s => s.id))
        .concat(this.props.moderators.map(m => m.user_id));
      return canMod(
        UserService.Instance.user,
        adminsThenSitemodsThenMods,
        this.props.node.comment.creator_id
      );
    } else {
      return false;
    }
  }

  get canAdmin(): boolean {
    return (
      this.props.admins &&
      canMod(
        UserService.Instance.user,
        this.props.admins.map(a => a.id),
        this.props.node.comment.creator_id
      )
    );
  }

  get canSitemod(): boolean {
    return (
      this.props.sitemods &&
      canMod(
        UserService.Instance.user,
        this.props.sitemods.map(s => s.id),
        this.props.node.comment.creator_id
      )
    );
  }

  get amCommunityCreator(): boolean {
    return (
      this.props.moderators &&
      UserService.Instance.user &&
      this.props.node.comment.creator_id != UserService.Instance.user.id &&
      UserService.Instance.user.id == this.props.moderators[0].user_id
    );
  }

  get amSiteCreator(): boolean {
    return (
      this.props.admins &&
      UserService.Instance.user &&
      this.props.node.comment.creator_id != UserService.Instance.user.id &&
      UserService.Instance.user.id == this.props.admins[0].id
    );
  }

  get commentUnlessRemoved(): string {
    let node = this.props.node;
    return node.comment.removed
      ? `*${i18n.t('removed')}*`
      : node.comment.deleted
      ? `*${i18n.t('deleted')}*`
      : node.comment.content;
  }

  formatInnerHTML(html: string) {
    html = imagesDownsize(mdToHtml(html).__html, false, true);
    if (!UserService.Instance.user || !UserService.Instance.user.show_nsfw) {
      html = replaceImageEmbeds(html);
    }
    return { __html: html };
  }

  handleReplyClick(i: CommentNode) {
    i.setState({
      showReply: true,
    });
  }

  handleEditClick(i: CommentNode) {
    i.setState({
      showEdit: true,
    });
  }

  handleDeleteClick(i: CommentNode) {
    let deleteForm: DeleteCommentForm = {
      edit_id: i.props.node.comment.id,
      deleted: !i.props.node.comment.deleted,
      auth: null,
    };
    WebSocketService.Instance.deleteComment(deleteForm);
  }

  handleSaveCommentClick(i: CommentNode) {
    let saved =
      i.props.node.comment.saved == undefined
        ? true
        : !i.props.node.comment.saved;
    let form: SaveCommentForm = {
      comment_id: i.props.node.comment.id,
      save: saved,
    };

    WebSocketService.Instance.saveComment(form);

    i.state.saveLoading = true;
    i.setState(this.state);
  }

  handleReplyCancel() {
    this.setState({
      showReply: false,
      showEdit: false,
    });
  }

  handleCommentUpvote(i: CommentNodeI) {
    let new_vote = this.state.my_vote == 1 ? 0 : 1;

    if (this.state.my_vote == 1) {
      this.state.score--;
      this.state.upvotes--;
    } else if (this.state.my_vote == -1) {
      this.state.downvotes--;
      this.state.upvotes++;
      this.state.score += 2;
    } else {
      this.state.upvotes++;
      this.state.score++;
    }

    this.state.my_vote = new_vote;

    let form: CommentLikeForm = {
      comment_id: i.comment.id,
      post_id: i.comment.post_id,
      score: this.state.my_vote,
    };

    WebSocketService.Instance.likeComment(form);
    this.setState(this.state);
    setupTippy();
  }

  handleCommentDownvote(i: CommentNodeI) {
    let new_vote = this.state.my_vote == -1 ? 0 : -1;

    if (this.state.my_vote == 1) {
      this.state.score -= 2;
      this.state.upvotes--;
      this.state.downvotes++;
    } else if (this.state.my_vote == -1) {
      this.state.downvotes--;
      this.state.score++;
    } else {
      this.state.downvotes++;
      this.state.score--;
    }

    this.state.my_vote = new_vote;

    let form: CommentLikeForm = {
      comment_id: i.comment.id,
      post_id: i.comment.post_id,
      score: this.state.my_vote,
    };

    WebSocketService.Instance.likeComment(form);
    this.setState(this.state);
    setupTippy();
  }

  handleModRemoveShow(i: CommentNode) {
    i.state.showRemoveDialog = true;
    i.setState(i.state);
  }

  handleModRemoveReasonChange(i: CommentNode, event: any) {
    i.state.removeReason = event.target.value;
    i.setState(i.state);
  }

  handleModRemoveSubmit = (event: React.SyntheticEvent) => {
    event.preventDefault();
    let form: RemoveCommentForm = {
      edit_id: this.props.node.comment.id,
      removed: !this.props.node.comment.removed,
      reason: this.state.removeReason,
      auth: null,
    };
    WebSocketService.Instance.removeComment(form);

    this.setState({ showRemoveDialog: false });
  };

  handleMarkRead = () => {
    // if it has a user_mention_id field, then its a mention
    if (this.props.node.comment.user_mention_id) {
      let form: EditUserMentionForm = {
        user_mention_id: this.props.node.comment.user_mention_id,
        read: !this.props.node.comment.read,
      };
      WebSocketService.Instance.editUserMention(form);
    } else {
      let form: MarkCommentReadForm = {
        edit_id: this.props.node.comment.id,
        read: !this.props.node.comment.read,
        auth: null,
      };
      WebSocketService.Instance.markCommentAsRead(form);
    }

    this.setState({ readLoading: true });
  };

  handleModBanFromCommunityShow(i: CommentNode) {
    i.state.showBanDialog = !i.state.showBanDialog;
    i.state.banType = BanType.Community;
    i.setState(i.state);
  }

  handleModBanShow(i: CommentNode) {
    i.state.showBanDialog = !i.state.showBanDialog;
    i.state.banType = BanType.Site;
    i.setState(i.state);
  }

  handleModBanReasonChange(i: CommentNode, event: any) {
    i.state.banReason = event.target.value;
    i.setState(i.state);
  }

  handleModBanExpiresChange(i: CommentNode, event: any) {
    i.state.banExpires = event.target.value;
    i.setState(i.state);
  }

  handleModBanFromCommunitySubmit(i: CommentNode) {
    i.state.banType = BanType.Community;
    i.setState(i.state);
    i.handleModBanBothSubmit(i);
  }

  handleModBanSubmit(i: CommentNode) {
    i.state.banType = BanType.Site;
    i.setState(i.state);
    i.handleModBanBothSubmit(i);
  }

  handleModBanBothSubmit(i: CommentNode) {
    event.preventDefault();

    if (i.state.banType == BanType.Community) {
      let form: BanFromCommunityForm = {
        user_id: i.props.node.comment.creator_id,
        community_id: i.props.node.comment.community_id,
        ban: !i.props.node.comment.banned_from_community,
        reason: i.state.banReason,
        expires: getUnixTime(i.state.banExpires),
      };
      WebSocketService.Instance.banFromCommunity(form);
    } else {
      let form: BanUserForm = {
        user_id: i.props.node.comment.creator_id,
        ban: !i.props.node.comment.banned,
        reason: i.state.banReason,
        expires: getUnixTime(i.state.banExpires),
      };
      WebSocketService.Instance.banUser(form);
    }

    i.state.showBanDialog = false;
    i.setState(i.state);
  }

  handleShowConfirmAppointAsMod(i: CommentNode) {
    i.state.showConfirmAppointAsMod = true;
    i.setState(i.state);
  }

  handleCancelConfirmAppointAsMod(i: CommentNode) {
    i.state.showConfirmAppointAsMod = false;
    i.setState(i.state);
  }

  handleAddModToCommunity(i: CommentNode) {
    let form: AddModToCommunityForm = {
      user_id: i.props.node.comment.creator_id,
      community_id: i.props.node.comment.community_id,
      added: !i.isMod,
    };
    WebSocketService.Instance.addModToCommunity(form);
    i.state.showConfirmAppointAsMod = false;
    i.setState(i.state);
  }

  handleShowConfirmAppointAsAdmin(i: CommentNode) {
    i.state.showConfirmAppointAsAdmin = true;
    i.setState(i.state);
  }

  handleCancelConfirmAppointAsAdmin(i: CommentNode) {
    i.state.showConfirmAppointAsAdmin = false;
    i.setState(i.state);
  }

  handleAddAdmin(i: CommentNode) {
    let form: AddAdminForm = {
      user_id: i.props.node.comment.creator_id,
      added: !i.isAdmin,
    };
    WebSocketService.Instance.addAdmin(form);
    i.state.showConfirmAppointAsAdmin = false;
    i.setState(i.state);
  }

  handleShowConfirmAppointAsSitemod(i: CommentNode) {
    i.state.showConfirmAppointAsSitemod = true;
    i.setState(i.state);
  }

  handleCancelConfirmAppointAsSitemod(i: CommentNode) {
    i.state.showConfirmAppointAsSitemod = false;
    i.setState(i.state);
  }

  handleAddSitemod(i: CommentNode) {
    let form: AddSitemodForm = {
      user_id: i.props.node.comment.creator_id,
      added: !i.isSitemod,
    };
    WebSocketService.Instance.addSitemod(form);
    i.state.showConfirmAppointAsSitemod = false;
    i.setState(i.state);
  }

  handleShowConfirmTransferCommunity(i: CommentNode) {
    i.state.showConfirmTransferCommunity = true;
    i.setState(i.state);
  }

  handleCancelShowConfirmTransferCommunity(i: CommentNode) {
    i.state.showConfirmTransferCommunity = false;
    i.setState(i.state);
  }

  handleTransferCommunity(i: CommentNode) {
    let form: TransferCommunityForm = {
      community_id: i.props.node.comment.community_id,
      user_id: i.props.node.comment.creator_id,
    };
    WebSocketService.Instance.transferCommunity(form);
    i.state.showConfirmTransferCommunity = false;
    i.setState(i.state);
  }

  handleShowConfirmTransferSite(i: CommentNode) {
    i.state.showConfirmTransferSite = true;
    i.setState(i.state);
  }

  handleCancelShowConfirmTransferSite(i: CommentNode) {
    i.state.showConfirmTransferSite = false;
    i.setState(i.state);
  }

  handleTransferSite(i: CommentNode) {
    let form: TransferSiteForm = {
      user_id: i.props.node.comment.creator_id,
    };
    WebSocketService.Instance.transferSite(form);
    i.state.showConfirmTransferSite = false;
    i.setState(i.state);
  }

  handleReportComment(i: CommentNode) {
    i.state.showReportDialog = !i.state.showReportDialog;
    i.setState(i.state);
  }

  handleReportReasonChange(i: CommentNode, event: any) {
    i.state.reportReason = event.target.value;
    i.setState(i.state);
  }

  handleReportSubmit(i: CommentNode, e: any) {
    e.preventDefault();

    WebSocketService.Instance.createCommentReport({
      comment: i.props.node.comment.id,
      reason: i.state.reportReason,
    });

    i.state.reportReason = null;
    i.state.showReportDialog = false;

    i.setState(i.state);
  }

  get isCommentNew(): boolean {
    let now = moment.utc().subtract(10, 'minutes');
    let then = moment.utc(this.props.node.comment.published);
    return now.isBefore(then);
  }

  handleCommentCollapse(i: CommentNode) {
    i.state.collapsed = !i.state.collapsed;
    i.setState(i.state);
  }

  handleViewSource(i: CommentNode) {
    i.state.viewSource = !i.state.viewSource;
    i.setState(i.state);
  }

  handleShowAdvanced(i: CommentNode) {
    i.state.showAdvanced = !i.state.showAdvanced;
    i.setState(i.state);
    setupTippy();
  }

  get scoreColor() {
    if (this.state.my_vote == 1) {
      return 'text-info';
    } else if (this.state.my_vote == -1) {
      return 'text-danger';
    } else {
      return 'text-muted';
    }
  }

  get pointsTippy(): string {
    let points = i18n.t('number_of_points', {
      count: this.state.score,
    });

    let upvotes = i18n.t('number_of_upvotes', {
      count: this.state.upvotes,
    });

    let downvotes = i18n.t('number_of_downvotes', {
      count: this.state.downvotes,
    });

    return `${points} • ${upvotes} • ${downvotes}`;
  }
}
