import React, { useState } from 'react';
import Sentry from '@sentry/react';
import Block from './elements/Block';
import { Heading, Text, Textarea, ThemeProvider, Link } from 'theme-ui';
import defaultTheme from '../theme';
import Button from './elements/Button';

// https://sentry.io/api/0/projects/{organization_slug}/{project_slug}/user-feedback/

// event_id (string) — the event ID
// name (string) — user's name
// email (string) — user's email address
// comments (string) — comments supplied by user

export default function ErrorFallback() {
  return (
    <ThemeProvider theme={defaultTheme}>
      <Block
        width="100vw"
        height="100vh"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Block maxWidth="400px">
          <Heading as="h3">Something Went Wrong</Heading>
          <Text my={1}>Help Us Out By Letting Us Know What Happened</Text>
          <Block my={3}>
            <Button
              as="a"
              css={{ width: '100%', color: '#fff !important' }}
              // @ts-ignore
              href="https://www.chapo.chat/create_post?community=feedback"
            >
              Create a Bug Report
            </Button>
          </Block>
        </Block>
      </Block>
    </ThemeProvider>
  );
}
