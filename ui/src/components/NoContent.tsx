import React from 'react';
import { Box } from 'theme-ui';
import { Icon } from './icon';

export default function NoContent({ children }) {
  return (
    <Box
      css={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '20px 0',
      }}
    >
      {children}
      <Icon name="hexbear" size="40px" />
    </Box>
  );
}
