import React, { useEffect } from 'react';

const useTitle = title => {
  useEffect(() => {
    document.title = title;
  }, [title]);
};

export default function DocumentTitle({ title }: { title?: string }): null {
  useTitle(title);
  return null;
}
