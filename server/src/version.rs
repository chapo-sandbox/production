pub const VERSION: &str = "v0.7.41";
pub const GIT_SHA: &str = env!("VERGEN_SHA");
pub const GIT_SHA_SHORT: &str = env!("VERGEN_SHA_SHORT");
pub const SEMVER_LIGHTWEIGHT: &str = env!("VERGEN_SEMVER_LIGHTWEIGHT");
pub const BUILD_TIMESTAMP: &str = env!("VERGEN_BUILD_TIMESTAMP");
pub const BUILD_DATE: &str = env!("VERGEN_BUILD_DATE");
